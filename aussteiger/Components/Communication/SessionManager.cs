﻿using System;
using System.Collections.Generic;

using aussteiger.DataObject;

namespace aussteiger.Components.Communication
{
    public class SessionManager
    {
        private readonly Dictionary<string, SessionInfo> _sessions = new Dictionary<string, SessionInfo>();

        public string CreatSession(string userId)
        {
            lock (_sessions)
            {
                var randomKey = GenerateNewRandomKey();
                var sessionInfo = new SessionInfo { UserId = userId };
                _sessions.Add(randomKey, sessionInfo);
                return randomKey;
            }
        }

        public SessionInfo GetSessionInfo(string sessionId)
        {
            if (sessionId == null) return null;

            lock (_sessions)
            {
                return _sessions.ContainsKey(sessionId) ? _sessions[sessionId] : null;
            }
        }

        public void EndSession(string sessionId)
        {
            if (sessionId == null) return;

            lock (_sessions)
            {
                if (_sessions.ContainsKey(sessionId))
                {
                    _sessions.Remove(sessionId);
                }
            }
        }

        private string GenerateNewRandomKey()
        {
            while (true)
            {
                var randomKey = Guid.NewGuid().ToString().Replace("-", String.Empty).Substring(0, 16);
                if (!_sessions.ContainsKey(randomKey)) return randomKey;
            }
        }
    }
}
