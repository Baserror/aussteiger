﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aussteiger.Components.Persistence.ORM;
using aussteiger.DataObject;
using DbLinq.Data.Linq;
using MySql.Data.MySqlClient;

namespace aussteiger.Components.Persistence
{
    class MySqlHelper
    {
        //private static readonly string ConnectionString = $"SERVER={Global.Settings.Server};DATABASE={Global.Settings.Database};UID={Global.Settings.UserID};PASSWORD={Global.Settings.Password};";
        private static readonly string ConnectionString = $"SERVER=localhost;DATABASE=aussteiger;UID=root;PASSWORD=1234;";

        public static Aussteiger GetDataContext()
        {
            return new Aussteiger(new MySqlConnection(ConnectionString));
        }
    }
}
