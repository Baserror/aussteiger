﻿using System;

namespace aussteiger.Attributes
{
    internal class UrlMapping : Attribute
    {
        public readonly string Map;
        public readonly string HttpMethod;

        public UrlMapping(string map, string httpMethod)
        {
            Map = map;
            HttpMethod = httpMethod;
        }
    }
}
