﻿namespace aussteiger.DataObject.REST
{
    class UserInfo
    {
        public string Mail { get; set; }
        public string Password { get; set; }
        public string RepeatPassword { get; set; }
        public string Name { get; set; }
        public string Shortcut { get; set; }
        public int? Department { get; set; }
    }
}
